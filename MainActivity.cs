﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using Test_Xamarin.Assets.Util;
using System.Collections.Generic;

namespace Test_Xamarin
{
    [Activity(Label = "Test Xamarin", MainLauncher = true)]
    public class MainActivity : Activity
    {

        TextView lblIdUser, lblPay, lblDebt, lblMsm;
        EditText txtIdUser, txtPay, txtDebt;
        Button btnSend;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Main);

            lblIdUser = FindViewById<TextView>(Resource.Id.lblIdUser);
            lblPay = FindViewById<TextView>(Resource.Id.lblPay);
            lblDebt = FindViewById<TextView>(Resource.Id.lblDebt);

            txtIdUser = FindViewById<EditText>(Resource.Id.txtIdUser);
            txtPay = FindViewById<EditText>(Resource.Id.txtPay);
            txtDebt = FindViewById<EditText>(Resource.Id.txtDebt);

            lblMsm = FindViewById<TextView>(Resource.Id.lblMsm);
            btnSend = FindViewById<Button>(Resource.Id.btnSend);

            btnSend.Click += btnSend_Click;
        }

        private async void  btnSend_Click(object sender, EventArgs e)
        {
            if (txtIdUser.Text == string.Empty)
            {
                lblMsm.Text = "Debe de ingresar su cédula de ciudadanía";
                return;
            }
            if (txtPay.Text == string.Empty)
            {
                lblMsm.Text = "Debe de ingresar su salario base";
                return;
            }
            if (txtDebt.Text == string.Empty)
            {
                lblMsm.Text = "Debe de ingresar el monto que desea prestar";
                return;
            }

            RestService service = new RestService();
            string Items = await service.RefreshDataAsync();
            lblMsm.Text = Items;

        }
    }
}

