﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Test_Xamarin.Assets.Util
{
    public class RestService
    {
        HttpClient client;
        public List<String> Items { get; private set; }

        public RestService()
        {
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
        }

        public async Task<String> RefreshDataAsync()
        {
            Items = new List<String>();
            String item = "";
            var uri = new Uri(string.Format(Config.RestUrl, string.Empty));

            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                item = content;
               // Items = JsonConvert.DeserializeObject<List<String>>(content);
            }

            return item;
        }
    }

}