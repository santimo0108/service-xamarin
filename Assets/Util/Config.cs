﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Test_Xamarin.Assets.Util
{
    public static class Config
    {
        public static string RestUrl = "https://bundyserver.azurewebsites.net/whois.aspx?beacon=00293970";
        // Credentials that are hard coded into the REST service
        public static string Username = "Xamarin";
        public static string Password = "Pa$$w0rd";
    }
}